require 'test_helper'

class ResidentialsControllerTest < ActionController::TestCase
  setup do
    @residential = residentials(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:residentials)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create residential" do
    assert_difference('Residential.count') do
      post :create, residential: { area_id: @residential.area_id, central_facility_id: @residential.central_facility_id, description: @residential.description, furniture_id: @residential.furniture_id, imageable_id: @residential.imageable_id, postable_id: @residential.postable_id, price: @residential.price, private_facility_id: @residential.private_facility_id, property_bathroom_id: @residential.property_bathroom_id, property_bedroom_id: @residential.property_bedroom_id, property_furnishing_type_id: @residential.property_furnishing_type_id, property_parking_space_id: @residential.property_parking_space_id, property_pet_friendly_type_id: @residential.property_pet_friendly_type_id, size: @residential.size, title: @residential.title }
    end

    assert_redirected_to residential_path(assigns(:residential))
  end

  test "should show residential" do
    get :show, id: @residential
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @residential
    assert_response :success
  end

  test "should update residential" do
    patch :update, id: @residential, residential: { area_id: @residential.area_id, central_facility_id: @residential.central_facility_id, description: @residential.description, furniture_id: @residential.furniture_id, imageable_id: @residential.imageable_id, postable_id: @residential.postable_id, price: @residential.price, private_facility_id: @residential.private_facility_id, property_bathroom_id: @residential.property_bathroom_id, property_bedroom_id: @residential.property_bedroom_id, property_furnishing_type_id: @residential.property_furnishing_type_id, property_parking_space_id: @residential.property_parking_space_id, property_pet_friendly_type_id: @residential.property_pet_friendly_type_id, size: @residential.size, title: @residential.title }
    assert_redirected_to residential_path(assigns(:residential))
  end

  test "should destroy residential" do
    assert_difference('Residential.count', -1) do
      delete :destroy, id: @residential
    end

    assert_redirected_to residentials_path
  end
end
