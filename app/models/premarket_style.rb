class PremarketStyle < ActiveRecord::Base
	belongs_to :premarket

	belongs_to :property_bedroom
	belongs_to :property_bathroom
	belongs_to :property_furnishing_type
	belongs_to :property_pet_friendly_type
	belongs_to :property_parking_space
	has_many :property_private_facilities, as: :private_facility
	has_many :property_furnitures, as: :furniture
	has_many :property_areas, as: :area
	has_many :property_galleries, as: :imageable, dependent: :destroy

	accepts_nested_attributes_for :property_galleries, reject_if: :all_blank, allow_destroy: :true 
end
