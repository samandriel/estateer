class UserProfession < ActiveRecord::Base
	belongs_to :user_profile
	has_one :user_company
	belongs_to :user_professional_expertise

	accepts_nested_attributes_for :user_company, reject_if: :all_blank
end
