class Residential < ActiveRecord::Base
  has_many :property_central_facilities, as: :central_facility
  has_many :property_private_facilities, as: :private_facility
  has_many :property_furnitures, as: :furniture
  has_many :property_areas, as: :area
  has_many :property_galleries, as: :imageable
  has_one :property_location, as: :locatable
  belongs_to :property_post_type
  belongs_to :residential_type
  belongs_to :property_bedroom
  belongs_to :property_bathroom
  belongs_to :property_furnishing_type
  belongs_to :property_pet_friendly_type
  belongs_to :property_parking_space

  accepts_nested_attributes_for :property_galleries, reject_if: :all_blank, allow_destroy: :true
  accepts_nested_attributes_for :property_location
  
  # Sunspot search function
  searchable do
    text :title, :description
    integer :residential_type_id 
    integer :property_post_type_id
    integer :property_bedroom_id
    integer :property_bathroom_id
    integer :property_furnishing_type_id
    integer :property_pet_friendly_type_id
    integer :property_parking_space_id
    integer :property_central_facility_ids, :multiple => true
    # integer :private_facility, :multiple => true
    # integer :furniture, :multiple => true
    # integer :area,  :multiple => true
    double :price
    double :size
  end
end
