class Premarket < ActiveRecord::Base
	belongs_to :premarket_type
	
	has_many :premarket_styles, dependent: :destroy
	has_many :property_central_facilities, as: :central_facility
	has_one :property_location, as: :locatable

  accepts_nested_attributes_for :property_location
	accepts_nested_attributes_for :premarket_styles, reject_if: :all_blank, allow_destroy: :true 

	mount_uploader :feature_image, PremarketFeatureImageUploader
end
