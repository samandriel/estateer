class Commercial < ActiveRecord::Base
  belongs_to :commercial_type
  belongs_to :property_post_type
  has_many :property_galleries, as: :imageable
  has_one :property_location, as: :locatable

  accepts_nested_attributes_for :property_galleries, reject_if: :all_blank, allow_destroy: :true
  accepts_nested_attributes_for :property_location

  # Sunspot search function
  searchable do
  	text :title, :description
  	integer :property_post_type_id
  	integer :commercial_type_id
  	double :price
  	double :size
  end
end