class PropertyFurnishingType < ActiveRecord::Base
	has_one :residential
	has_one :premarket_style
end
