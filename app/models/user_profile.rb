class UserProfile < ActiveRecord::Base
	belongs_to :user
	has_one :user_profession

	accepts_nested_attributes_for :user_profession, reject_if: :all_blank 
	
end
