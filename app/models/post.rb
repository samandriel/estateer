class Post < ActiveRecord::Base
	has_many :post_categorizations
	has_many :post_categories, through: :post_categorizations

	mount_uploader :feature_image, PostFeatureImageUploader

	# Search function
	searchable do 
		text :title, :excerpt, :content
		integer :post_category_ids, :multiple => true
	end
end
