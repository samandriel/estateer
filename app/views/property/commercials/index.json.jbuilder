json.array!(@commercials) do |commercial|
  json.extract! commercial, :id, :title, :postable_id, :commercial_type_id, :price, :size, :description, :imageable_id
  json.url commercial_url(commercial, format: :json)
end
