json.array!(@residentials) do |residential|
  json.extract! residential, :id, :title, :postable_id, :price, :size, :description, :property_bedroom_id, :property_bathroom_id, :property_furnishing_type_id, :property_pet_friendly_type_id, :property_parking_space_id, :central_facility_id, :private_facility_id, :furniture_id, :area_id, :imageable_id
  json.url residential_url(residential, format: :json)
end
