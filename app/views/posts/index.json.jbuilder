json.array!(@posts) do |post|
  json.extract! post, :id, :title, :post_category_id, :excerpt, :content, :feature_image, :admin_id
  json.url post_url(post, format: :json)
end
