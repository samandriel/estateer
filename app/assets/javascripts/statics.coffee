# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).foundation({
  orbit: {
      animation: 'fade',
      timer_speed: 3000,
      pause_on_hover: false,
      resume_on_mouseout: false,
      next_on_click: false,
      animation_speed: 1000,
      stack_on_small: false,
      navigation_arrows: false,
      slide_number: false,
      slide_number_text: 'of',
      container_class: 'orbit-container',
      stack_on_small_class: 'orbit-stack-on-small',
      next_class: 'orbit-next',
      prev_class: 'orbit-prev',
      timer_container_class: 'orbit-timer',
      timer_paused_class: 'paused',
      timer_progress_class: 'orbit-progress',
      slides_container_class: 'orbit-slides-container',
      preloader_class: 'preloader',
      slide_selector: 'li',
      bullets_container_class: 'orbit-bullets',
      bullets_active_class: 'active'
      slide_number_class: 'orbit-slide-number'
      caption_class: 'orbit-caption'
      active_slide_class: 'active'
      orbit_transition_class: 'orbit-transitioning',
      bullets: false,
      circular: true,
      timer: false,
      variable_height: false,
      swipe: true,
      before_slide_change: noop,
      after_slide_change: noop
  }
});