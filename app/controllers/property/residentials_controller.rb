class Property::ResidentialsController < ApplicationController
  before_action :set_residential, only: [:show, :edit, :update, :destroy]

  def index

    @residentials = Residential.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    @search = Residential.search do
      fulltext params[:search_residential_property]
      with(:property_post_type_id, params[:property_post_type]) if params[:property_post_type].present?
      with(:residential_type_id, params[:residential_type]) if params[:residential_type].present?
      with(:price).between(params[:price_min]..params[:price_max]) if params[:price_min].present? or params[:price_max].present?
      with(:size).between(params[:size_min]..params[:size_max]) if params[:size_min].present? or params[:size_max].present?
      with(:property_bedroom_id).between(params[:property_bedroom_min]..params[:property_bedroom_max]) if params[:property_bedroom_min].present? or params[:property_bedroom_max].present?
      with(:property_bathroom_id).between(params[:property_bathroom_min]..params[:property_bathroom_max]) if params[:property_bathroom_min].present? or params[:property_bathroom_max].present?
      with(:property_pet_friendly_type_id, params[:property_pet_friendly_type]) if params[:property_pet_friendly_type].present?
      with(:property_furnishing_type_id, params[:property_furnishing_type]) if params[:property_furnishing_type].present?
      with(:property_parking_space_id, params[:property_parking_space]) if params[:property_parking_space].present?
      with(:property_central_facility_ids).any_of(params[[:central_facility]]) if params[:central_facility].present?
    end
    @residentials = Residential.where(id: @search.results.map(&:id)).paginate(:page => params[:page], :per_page => 10)
  end

  def show
  end

  def new
    @residential = Residential.new
    property_location = @residential.build_property_location
  end

  def edit
  end

  def create
    @residential = Residential.new(residential_params)

    respond_to do |format|
      if @residential.save
        gallery_images
        format.html { redirect_to @residential, notice: 'Residential was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @residential.update(residential_params)
        gallery_images
        format.html { redirect_to @residential, notice: 'Residential was successfully updated.' }
        format.json { render :show, status: :ok, location: @residential }
      else
        format.html { render :edit }
        format.json { render json: @residential.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @residential.destroy
    respond_to do |format|
      format.html { redirect_to residentials_url, notice: 'Residential was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def gallery_images
        if  params[:property_galleries]['image'].each do |obj|
          @residential.property_galleries.create!(image: obj)
        end
      end
    end

    def set_residential
      @residential = Residential.find(params[:id])
    end

    def residential_params
      params.require(:residential).permit(
        :title,
        :residential_type_id,
        :property_post_type_id,
        :price,
        :size,
        :description,
        :property_bedroom_id,
        :property_bathroom_id,
        :property_furnishing_type_id,
        :property_pet_friendly_type_id,
        :property_parking_space_id,
        {property_central_facility_ids: []},
        {property_private_facility_ids: []},
        {property_furniture_ids: []},
        {property_area_ids: []},
        property_galleries_attributes: [
          :id,
          :image,
          :_destroy
        ],
        property_location_attributes: [
          :id,
          :address,
          :street,
          :city,
          :province,
          :country,
          :postal_code,
          :latitude,
          :longitude
        ]
      )
    end
end
