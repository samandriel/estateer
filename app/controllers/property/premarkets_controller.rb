class Property::PremarketsController < ApplicationController
  before_action :set_premarket, only: [:show, :edit, :update, :destroy]
  
  def index
      @premarkets = Premarket.all.order("created_at DESC")    
  end

  def show
  end

  def new
    @premarket = Premarket.new
    property_location = @premarket.build_property_location
    premarket_style = @premarket.premarket_styles.build
  end

  def edit
  end

  def create
    @premarket = Premarket.new(premarket_params)

    respond_to do |format|
      if @premarket.save
        format.html { redirect_to @premarket, notice: "Premarket has been created" }
      else
        format.html { render :new }
      end
    end

  end

  def update
    respond_to do |format|
      if @premarket.update(premarket_params)
        format.html { redirect_to @premarket, notice: "Premarket has been updated"}
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @premarket.destroy

    redirect_to do |format|
      format.html { redirect_to premarkets_url, notice: "Premarket has been deleted"}
    end
  end

  private

    def set_premarket
      @premarket = Premarket.find(params[:id])
    end

    def premarket_params
      params.require(:premarket).permit(
        :title,
        :premarket_type_id,
        :price,
        :description,
        :special_offer,
        :feature_image,
        {property_central_facility_ids: []},
        property_location_attributes: [
          :id,
          :address,
          :street,
          :city,
          :province,
          :country,
          :postal_code,
          :latitude,
          :longitude
        ]
      )
    end


end
