class Property::CommercialsController < ApplicationController
  before_action :set_commercial, only: [:show, :edit, :update, :destroy]

  def index
      @commercials = Commercial.all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
      @search = Commercial.search do
        fulltext params[:search_commercial_property]
        with(:property_post_type_id, params[:property_post_type]) if params[:property_post_type].present?
        with(:commercial_type_id, params[:commercial_type]) if params[:commercial_type].present?
        with(:price).between(params[:price_min]..params[:price_max]) if params[:price_min].present? or params[:price_ma].present?
        with(:size).between(params[:size_min]..params[:size_max]) if params[:size_min].present? or params[:size_max].present?
      end
      @search = Commercial.where(id: @search.results.map(&:id)).paginate(page: params[:page], per_page: 10)
  end

  def show
  end

  def new
    @commercial = Commercial.new
    property_location = @commercial.build_property_location
  end

  def edit
  end

  def create
    @commercial = Commercial.new(commercial_params)

    respond_to do |format|
      if @commercial.save
        gallery_images
        format.html { redirect_to @commercial, notice: 'Commercial was successfully created.' }
        format.json { render :show, status: :created, location: @commercial }
      else
        format.html { render :new }
        format.json { render json: @commercial.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @commercial.update(commercial_params)
        gallery_images
        format.html { redirect_to @commercial, notice: 'Commercial was successfully updated.' }
        format.json { render :show, status: :ok, location: @commercial }
      else
        format.html { render :edit }
        format.json { render json: @commercial.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @commercial.destroy
    respond_to do |format|
      format.html { redirect_to property_commercials_url, notice: 'Commercial was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def gallery_images
        if  params[:property_galleries]['image'].each do |obj|
          @commercial.property_galleries.create!(image: obj)
        end
      end
    end

    def set_commercial
      @commercial = Commercial.find(params[:id])
    end

    def commercial_params
      params.require(:commercial).permit(
        :title, 
        :property_post_type_id, 
        :commercial_type_id, 
        :price, 
        :size, 
        :description, 
        property_galleries_attributes: [
          :id,
          :image  
        ],
        property_location_attributes: [
          :id,
          :address,
          :street,
          :city,
          :province,
          :country,
          :postal_code,
          :latitude,
          :longitude
        ]
      )
    end
end
