class Property::PremarketStylesController < ApplicationController
	before_action :set_premarket
	before_action :set_premarket_style, except: [:new, :create]

	def new
		@premarket_style = PremarketStyle.new
		@property_gallery = @premarket_style.property_galleries.build
	end

	def edit
	end

	def create
		@premarket_style = PremarketStyle.new(premarket_style_params)
		@premarket_style.premarket_id = params[:premarket_id]

		respond_to do |format|
			if @premarket_style.save
				gallery_images
				format.html { redirect_to premarket_url(@premarket), notice: "Premarket has been created" }
			else
				format.html { render :new }
			end
		end		
	end

	def update
		respond_to do |format|
			if @premarket_style.update(premarket_style_params)
				gallery_images
				format.html { redirect_to premarket_url(@premarket), notice: "Premarket has been updated"}
			else
				format.html { render :edit }
			end
		end
	end
	
	def destroy
		@premarket_style.destroy

		redirect_to do |format|
			format.html { redirect_to premarket_url(@premarket), notice: "Premarket has been deleted"}
		end
	end

	private

	    def gallery_images
      		params[:property_galleries]['image'].each do |obj|
				@premarket_style.property_galleries.create!(image: obj)
			end
	    end

		def set_premarket
			@premarket = Premarket.find(params[:premarket_id])
		end

		def set_premarket_style
			@premarket_style = @premarket.premarket_style.find(params[:id])
		end

		def premarket_style_params
			params.require(:premarket_style).permit(
				:premarket_id,
				:name,
				:price,
				:size,
				:description,
				:property_bedroom_id,
				:property_bathroom_id,
				:property_furnishing_type_id,
				:property_pet_friendly_type_id,
				:property_parking_space_id,
				{property_furniture_ids: []},
				{property_private_facility_ids: []},
				{property_area_ids: []},
				property_galleries_attributes: [
					:id,
					:image
				]
			)
		end
end
