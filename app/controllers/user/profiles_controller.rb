class User::ProfilesController < ApplicationController
  before_action :set_profile, except: [:new, :create]
  before_action :authenticate_user!, except: [:show]

  def show
  end

  def new
    @profile = current_user.build_user_profile
    user_profession = @profile.build_user_profession
    user_company = user_profession.build_user_company
  end

  def edit
  end

  def create
    @profile = UserProfile.new(profile_params)
    @profile.id = params[:user_id]
    @profile.user_id = params[:user_id]

    if @profile.save
      redirect_to root_path, notice: 'Profile has been created'
    else
      render :new
    end
  end

  def update
    if @profile.update(profile_params)
      redirect_to user_profile_path, notice: 'Profile has been updated'
    else
      render :edit
    end  
  end

  private

    def set_profile
      @profile = UserProfile.find(params[:id])
    end

    def profile_params
      params.require(:user_profile).permit(
        :first_name,
        :last_name,
        :email,
        :phone,
        :professional,
        :profile_image,
        user_profession_attributes: [
          :user_profile_id,
          :user_professional_expertise_id,
          :about_me,
          user_company_attributes: [
            :user_profession_id,
            :name,
            :website,
            :email,
            :phone,
            :fax,
            :address,
            :street,
            :city,
            :province,
            :country,
            :postal_code,
            :longitude,
            :latitude            
          ]
        ]
      )
    end

end
