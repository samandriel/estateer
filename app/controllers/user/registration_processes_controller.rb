class User::RegistrationProcessesController < ApplicationController
	include Wicked::Wizard

	steps :create_user, :create_personal_profile, :create_company_profile
end
