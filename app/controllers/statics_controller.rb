class StaticsController < ApplicationController

  def homepage
    @residential_search = Residential.search(params[:q])
    @commercial_search = Commercial.search(params[:q])
    @premarket_search = Premarket.search(params[:q])

    if @residential_search
      @residentials = @residential_search.result(distinct: true)
    elsif @commercial_search
      @commercials = @commercial_search.result(distinct: true)
    else @premarket_search
      @premarkets = @premarket_search.result(distinct: true)
    end

  end 

end
