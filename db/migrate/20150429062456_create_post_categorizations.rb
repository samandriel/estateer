class CreatePostCategorizations < ActiveRecord::Migration
  def change
    create_table :post_categorizations do |t|
    	t.integer :post_id
    	t.integer :post_category_id

      t.timestamps null: false
    end
  end
end
