class CreateCommercials < ActiveRecord::Migration
  def change
    create_table :commercials do |t|
      t.string :title
      t.integer :property_post_type_id
      t.integer :commercial_type_id
      t.decimal :price
      t.decimal :size
      t.text :description
      t.references :imageable, polymorphic: true, index: true
      t.references :locatable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
