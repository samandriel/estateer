class CreatePropertyCentralFacilities < ActiveRecord::Migration
  def change
    create_table :property_central_facilities do |t|
      t.string :name
      t.integer :central_facility_id
      t.string :central_facility_type

      t.timestamps null: false
    end

    add_index :property_central_facilities, :central_facility_id
  end
end
