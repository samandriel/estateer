class CreatePremarkets < ActiveRecord::Migration
  def change
    create_table :premarkets do |t|
      t.string :title
      t.integer :premarket_type_id
      t.decimal :price
      t.text :description
      t.text :special_offer
      t.string :feature_image
      t.references :central_facility, polymorphic: true
      t.references :locatable, polymorphic: true, index: true

      t.timestamps null: false
    end

    add_index( :premarkets, [:central_facility_id, :central_facility_type], unique: true, name: 'premarket_central_facility')
  end
end
