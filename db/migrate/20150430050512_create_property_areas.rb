class CreatePropertyAreas < ActiveRecord::Migration
  def change
    create_table :property_areas do |t|
      t.string :name
      t.integer :area_id
      t.string :area_type

      t.timestamps null: false
    end

    add_index :property_areas, :area_id
  end
end
