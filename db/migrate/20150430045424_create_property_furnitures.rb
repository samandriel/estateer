class CreatePropertyFurnitures < ActiveRecord::Migration
  def change
    create_table :property_furnitures do |t|
      t.string :name
      t.integer :furniture_id
      t.string :furniture_type

      t.timestamps null: false
    end

    add_index :property_furnitures, :furniture_id
  end
end
