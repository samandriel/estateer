class CreateUserProfessions < ActiveRecord::Migration
  def change
    create_table :user_professions do |t|
    	t.integer :user_profile_id
    	t.integer :expertise
    	t.text :about_me


      t.timestamps null: false
    end
  end
end
