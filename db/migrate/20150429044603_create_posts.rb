class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.integer :post_category_id
      t.text :excerpt
      t.text :content
      t.string :feature_image
      t.integer :admin_id

      t.timestamps null: false
    end
  end
end
