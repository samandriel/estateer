class CreatePropertyLocations < ActiveRecord::Migration
  def change
    create_table :property_locations do |t|
      t.string :address
      t.string :street
      t.string :city
      t.string :province
      t.string :country
      t.string :postal_code
      t.float :longitude
      t.float :latitude
      t.integer :locatable_id
      t.string :locatable_type

      t.timestamps null: false
    end

    add_index :property_locations, :locatable_id
  end
end
