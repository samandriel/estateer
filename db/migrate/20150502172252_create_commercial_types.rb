class CreateCommercialTypes < ActiveRecord::Migration
  def change
    create_table :commercial_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
