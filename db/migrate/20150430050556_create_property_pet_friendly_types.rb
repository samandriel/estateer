class CreatePropertyPetFriendlyTypes < ActiveRecord::Migration
  def change
    create_table :property_pet_friendly_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
