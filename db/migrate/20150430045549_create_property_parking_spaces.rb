class CreatePropertyParkingSpaces < ActiveRecord::Migration
  def change
    create_table :property_parking_spaces do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
