class CreatePropertyBedrooms < ActiveRecord::Migration
  def change
    create_table :property_bedrooms do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
