class CreatePropertyGalleries < ActiveRecord::Migration
  def change
    create_table :property_galleries do |t|
    	t.string :image
    	t.integer :imageable_id
    	t.string :imageable_type 

      t.timestamps null: false
    end

    add_index :property_galleries, :imageable_id
  end
end
