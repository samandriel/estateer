class CreatePremarketStyles < ActiveRecord::Migration
  def change
    create_table :premarket_styles do |t|
      t.integer :premarket_id
      t.string :name
      t.decimal :price
      t.decimal :size
      t.text :description
      t.integer :property_bedroom_id
      t.integer :property_bathroom_id
      t.integer :property_furnishing_type_id
      t.integer :property_pet_friendly_type_id
      t.integer :property_parking_space_id
      t.references :furniture, polymorphic: true, index: true
      t.references :private_facility, polymorphic: true
      t.references :area, polymorphic: true, index: true
      t.references :imageable, polymorphic: true, index: true

      t.timestamps null: false
    end

    add_index( :premarket_styles, [:private_facility_id, :private_facility_type], unique: true, name: 'premarket_style_private_facility')
  end
end
