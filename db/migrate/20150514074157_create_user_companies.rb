class CreateUserCompanies < ActiveRecord::Migration
  def change
    create_table :user_companies do |t|
      t.integer :user_profession_id
      t.string :name
      t.string :website
      t.string :email
      t.string :phone
      t.string :fax
      t.string :address
      t.string :street
      t.string :city
      t.string :province
      t.string :country
      t.string :postal_code
      t.float :longitude
      t.float :latitude
      
      t.timestamps null: false
    end
  end
end
