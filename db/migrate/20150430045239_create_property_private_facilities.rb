class CreatePropertyPrivateFacilities < ActiveRecord::Migration
  def change
    create_table :property_private_facilities do |t|
      t.string :name
      t.integer :private_facility_id
      t.string :private_facility_type

      t.timestamps null: false
    end

    add_index :property_private_facilities, :private_facility_id
  end
end
