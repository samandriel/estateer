class CreatePropertyBathrooms < ActiveRecord::Migration
  def change
    create_table :property_bathrooms do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
