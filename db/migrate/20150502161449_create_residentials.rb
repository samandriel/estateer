class CreateResidentials < ActiveRecord::Migration
  def change
    create_table :residentials do |t|
      t.string :title
      t.integer :residential_type_id
      t.integer :property_post_type_id
      t.decimal :price
      t.decimal :size
      t.text :description
      t.integer :property_bedroom_id
      t.integer :property_bathroom_id
      t.integer :property_furnishing_type_id
      t.integer :property_pet_friendly_type_id
      t.integer :property_parking_space_id
      t.references :central_facility, polymorphic: true
      t.references :private_facility, polymorphic: true
      t.references :furniture, polymorphic: true, index: true
      t.references :area, polymorphic: true, index: true
      t.references :imageable, polymorphic: true, index: true
      t.references :locatable, polymorphic: true, index: true 

      t.timestamps null: false
    end

    add_index( :residentials, [:central_facility_id, :central_facility_type], unique: true, name: 'residential_central_facility')
    add_index( :residentials, [:private_facility_id, :private_facility_type], unique: true, name: 'residential_private_facility')
  end
end
