class CreateUserProfessionalExpertises < ActiveRecord::Migration
  def change
    create_table :user_professional_expertises do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
