# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150518044300) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "commercial_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "commercials", force: :cascade do |t|
    t.string   "title"
    t.integer  "property_post_type_id"
    t.integer  "commercial_type_id"
    t.decimal  "price"
    t.decimal  "size"
    t.text     "description"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.integer  "locatable_id"
    t.string   "locatable_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "commercials", ["imageable_type", "imageable_id"], name: "index_commercials_on_imageable_type_and_imageable_id", using: :btree
  add_index "commercials", ["locatable_type", "locatable_id"], name: "index_commercials_on_locatable_type_and_locatable_id", using: :btree

  create_table "post_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_categorizations", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "post_category_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.integer  "post_category_id"
    t.text     "excerpt"
    t.text     "content"
    t.string   "feature_image"
    t.integer  "admin_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "premarket_styles", force: :cascade do |t|
    t.integer  "premarket_id"
    t.string   "name"
    t.decimal  "price"
    t.decimal  "size"
    t.text     "description"
    t.integer  "property_bedroom_id"
    t.integer  "property_bathroom_id"
    t.integer  "property_furnishing_type_id"
    t.integer  "property_pet_friendly_type_id"
    t.integer  "property_parking_space_id"
    t.integer  "furniture_id"
    t.string   "furniture_type"
    t.integer  "private_facility_id"
    t.string   "private_facility_type"
    t.integer  "area_id"
    t.string   "area_type"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "premarket_styles", ["area_type", "area_id"], name: "index_premarket_styles_on_area_type_and_area_id", using: :btree
  add_index "premarket_styles", ["furniture_type", "furniture_id"], name: "index_premarket_styles_on_furniture_type_and_furniture_id", using: :btree
  add_index "premarket_styles", ["imageable_type", "imageable_id"], name: "index_premarket_styles_on_imageable_type_and_imageable_id", using: :btree
  add_index "premarket_styles", ["private_facility_id", "private_facility_type"], name: "premarket_style_private_facility", unique: true, using: :btree

  create_table "premarket_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "premarkets", force: :cascade do |t|
    t.string   "title"
    t.integer  "premarket_type_id"
    t.decimal  "price"
    t.text     "description"
    t.text     "special_offer"
    t.string   "feature_image"
    t.integer  "central_facility_id"
    t.string   "central_facility_type"
    t.integer  "locatable_id"
    t.string   "locatable_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "premarkets", ["central_facility_id", "central_facility_type"], name: "premarket_central_facility", unique: true, using: :btree
  add_index "premarkets", ["locatable_type", "locatable_id"], name: "index_premarkets_on_locatable_type_and_locatable_id", using: :btree

  create_table "property_areas", force: :cascade do |t|
    t.string   "name"
    t.integer  "area_id"
    t.string   "area_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "property_areas", ["area_id"], name: "index_property_areas_on_area_id", using: :btree

  create_table "property_bathrooms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_bedrooms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_central_facilities", force: :cascade do |t|
    t.string   "name"
    t.integer  "central_facility_id"
    t.string   "central_facility_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "property_central_facilities", ["central_facility_id"], name: "index_property_central_facilities_on_central_facility_id", using: :btree

  create_table "property_furnishing_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_furnitures", force: :cascade do |t|
    t.string   "name"
    t.integer  "furniture_id"
    t.string   "furniture_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "property_furnitures", ["furniture_id"], name: "index_property_furnitures_on_furniture_id", using: :btree

  create_table "property_galleries", force: :cascade do |t|
    t.string   "image"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "property_galleries", ["imageable_id"], name: "index_property_galleries_on_imageable_id", using: :btree

  create_table "property_locations", force: :cascade do |t|
    t.string   "address"
    t.string   "street"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "postal_code"
    t.float    "longitude"
    t.float    "latitude"
    t.integer  "locatable_id"
    t.string   "locatable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "property_locations", ["locatable_id"], name: "index_property_locations_on_locatable_id", using: :btree

  create_table "property_parking_spaces", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_pet_friendly_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_post_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "property_private_facilities", force: :cascade do |t|
    t.string   "name"
    t.integer  "private_facility_id"
    t.string   "private_facility_type"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "property_private_facilities", ["private_facility_id"], name: "index_property_private_facilities_on_private_facility_id", using: :btree

  create_table "residential_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "residentials", force: :cascade do |t|
    t.string   "title"
    t.integer  "residential_type_id"
    t.integer  "property_post_type_id"
    t.decimal  "price"
    t.decimal  "size"
    t.text     "description"
    t.integer  "property_bedroom_id"
    t.integer  "property_bathroom_id"
    t.integer  "property_furnishing_type_id"
    t.integer  "property_pet_friendly_type_id"
    t.integer  "property_parking_space_id"
    t.integer  "central_facility_id"
    t.string   "central_facility_type"
    t.integer  "private_facility_id"
    t.string   "private_facility_type"
    t.integer  "furniture_id"
    t.string   "furniture_type"
    t.integer  "area_id"
    t.string   "area_type"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.integer  "locatable_id"
    t.string   "locatable_type"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "residentials", ["area_type", "area_id"], name: "index_residentials_on_area_type_and_area_id", using: :btree
  add_index "residentials", ["central_facility_id", "central_facility_type"], name: "residential_central_facility", unique: true, using: :btree
  add_index "residentials", ["furniture_type", "furniture_id"], name: "index_residentials_on_furniture_type_and_furniture_id", using: :btree
  add_index "residentials", ["imageable_type", "imageable_id"], name: "index_residentials_on_imageable_type_and_imageable_id", using: :btree
  add_index "residentials", ["locatable_type", "locatable_id"], name: "index_residentials_on_locatable_type_and_locatable_id", using: :btree
  add_index "residentials", ["private_facility_id", "private_facility_type"], name: "residential_private_facility", unique: true, using: :btree

  create_table "user_companies", force: :cascade do |t|
    t.integer  "user_profession_id"
    t.string   "name"
    t.string   "website"
    t.string   "email"
    t.string   "phone"
    t.string   "fax"
    t.string   "address"
    t.string   "street"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "postal_code"
    t.float    "longitude"
    t.float    "latitude"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "user_professional_expertises", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_professions", force: :cascade do |t|
    t.integer  "user_profile_id"
    t.integer  "expertise"
    t.text     "about_me"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.boolean  "professional"
    t.string   "profile_image"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
