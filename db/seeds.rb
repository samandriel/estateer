# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

post_categories = ["News", "Event", "Article"]
post_categories.each{|obj| PostCategory.where(name: obj).first_or_create!}

# Property 
property_post_types = ["Rent", "Sell", "Rent-to-Own"]
property_post_types.each{|obj| PropertyPostType.where(:name => obj).first_or_create!}

residential_types = ["House", "Townhouse", "Condominium"]
residential_types.each{|obj| ResidentialType.where(:name => obj).first_or_create!}

commercial_types = ["Office", "Land", "Warehouse", "Agricultural"]
commercial_types.each{|obj| CommercialType.where(:name => obj).first_or_create }

premarket_types = ["Townhouse", "Condominium"]
premarket_types.each{|obj| PremarketType.where(:name => obj).first_or_create!}

property_bedrooms = ["Studio", "1 Bedroom", "2 Bedrooms", "3 Bedrooms", "4 Bedrooms", "5 Bedrooms", "6 Bedrooms", "7 Bedrooms", "8 Bedrooms", "9 Bedrooms", "10+ Bedrooms" ]
property_bedrooms.each{|obj| PropertyBedroom.where(:name => obj).first_or_create!}

property_bathrooms = [ "1 Bathroom", "2 Bathrooms", "3 Bathrooms", "4 Bathrooms", "5 Bathrooms", "6 Bathrooms", "7 Bathrooms", "8 Bathrooms", "9 Bathrooms", "10+ Bathrooms" ]
property_bathrooms.each{|obj| PropertyBathroom.where(:name => obj).first_or_create!}

property_furnishing_type = [ "Unfurnished", "Part Furnished", "Fully Furnished"]
property_furnishing_type.each{|obj| PropertyFurnishingType.where(:name => obj).first_or_create!}

property_pet_friendly_types = [ "No", "Yes", "Cat Only", "Dog Only"]
property_pet_friendly_types.each{|obj| PropertyPetFriendlyType.where(:name => obj).first_or_create!}

property_parking_spaces = [ "No", "1 Car", "2 Cars", "3 Cars", "4 Cars", "5 Cars", "6 Cars", "7 Cars", "8 Cars", "9 Cars", "10+ Cars" ]
property_parking_spaces.each{|obj| PropertyParkingSpace.where(:name => obj).first_or_create!}

property_central_facilities = [ "Wifi Hotspot", "Fitness", "Sauna", "Playground", "Swimming Pool", "Elevator", "Laundry", "Minimart", "Shuttle Bus"]
property_central_facilities.each{|obj| PropertyCentralFacility.where(:name => obj).first_or_create!}

property_private_facilities = ["Private Internet", "Cable TV", "Landline Telephone", "Air Conditioner", "Refrigerator", "Water Heater", "Extractor Hood", "Television", "Washing Machine", "Dryer Machine"]
property_private_facilities.each{|obj| PropertyPrivateFacility.where(:name => obj).first_or_create!}

property_furnitures = ["Sofa", "Coffee Table", "Dinning Table", "TV Stand", "Working Desk", "Bed", "Closet"]
property_furnitures.each{|obj| PropertyFurniture.where(:name => obj).first_or_create!}

property_areas = ["Kitchen", "Balcony", "Attic", "Basement", "Storage Room", "Front Yard", "Back Yard"]
property_areas.each{|obj| PropertyArea.where(:name => obj).first_or_create!}

# user profile 
user_professional_expertises = ["Residential Properties", "Commercial Properties", "Service Provider", "House/Townhouse", "Condominium", "Land"]
user_professional_expertises.each{|obj| UserProfessionalExpertise.where(name: obj).first_or_create!}